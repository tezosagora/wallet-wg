# Tezos Foundation

# Wallet Working Group Charter [Proposed]

## Preamble

The Tezos Ecosystem has seen the growth of many independent wallets
applications. This is to be encouraged and applauded. The diversity in
wallet applications brings vibrance and resiliency. It also--however--presents
challenges by showing disconnects and possible vulnerabilities. This makes a
large degree of interoperability either difficult or impossible across wallets
and/or other applications and systems.

The desired state is a thriving collection of varied wallet applications that
meet a common set of well-defined standards as well as established
best-practices and guidelines to aid in growing and maintaining the ecosystem.

## Summary

This document ("Charter") is the founding document, authorized by the Tezos
Foundation ("TF"), which establishes the Wallet Working Group ("WWG").

The Charter enumerates the mandate and process with which the WWG shall
operate.

## Mandate

The WWG is charged with the goal of maximizing interoperability and stability
of the Tezos wallet ecosystem.

This is to be achieved in the following ways:

### 1. To Establish Standards

Standards are prescribed rules, which are expcted to be adhered to by Wallet
Application Developers. They are to be established and maintained with the
following goals in mind:

 * Protect Tezos' interests
 * Protect users' interests
 * Ensuring a good degree of reliability
 * Ensuring a good degree of security

Standards are to be defined in the following areas of interest:

 * Security Standard
 * Indexer Client Standard
 * General

### 2. To Establish Guidelines

Guidelines are a collection of non-binding Best Practices to guide developers.
They are to be established and maintained with the following goals in mind:

 * Avoiding known pitfalls
 * Optimizing usability and user experience

Guidelines are to be established in the following areas of interest:

 * Feature Implementation Best Practices
 * User Experience Guidelines

### 3. To Publish and Maintain Helpful Material to Developers

Supporting Material is to be drafted, approved, published and maintained; e.g.:

 * Indexer Interface Specifications
 * Node Interaction Specifications

### 4. To Advise Developers

In support of developers, ad hoc queries are to be addressed; e.g.:

 * collect feedback;
 * collect requests;
 * answer clarifications on Standards and/or Guidelines.

### [FUTURE] 5. To Assess Applications

Developers do not incur direct penalties for not adhering to standards--their
application may likely encounter problems in the future. This may harm users;
therefore, the TF has interest in protecting users as well as incentivizing
good practices by developers.

To this end, the WWG may [in the future--as proposed] review and issue
certification/endorsement for wallet applications--as a statement that the
application meets the Standards, as defined by the WWG.

## Organization

### Creation

The Wallet Working Group is authorized and established by the Tezos
Foundation's Technology Advisor Committee.

### Composition

The Wallet Working Group is layered to streamline the efforts in producing
drafts and collecting feedback in a structure and organized way.

1. Board of Directors ("Board"): 3 members who are accountable for setting
Policy and Process of the WWG
1. Drafting Committee ("DC"): a small group (5-7 people) of members who draft
and edit proposals for Standards
1. Working Group ("WG"): a large number (40~50 people) of members who may
comment and vote on Standards

### Activities

1. Research: inquire, discover, discuss and consult on Wallet-related issues,
features, solutions and developments
1. Respond: requests/inquests from members
1. Draft: Submit TZIPs as Wallet Standards
1. Publish: Guidelines and other helpful material
1. Organize: round-tables with Wallet developers

## Process

### Board Meeting

The Board shall make resolutions on setting direction, with regards to
prioritizing subjects for the WWG on which to focus.

The Board shall meet weekly to discuss direction.

### Assignment

The Board may assign a topic to a member of the DC, who will be refered
to as the Lead.

Said Lead will be answerable for the progress of their assigned research topic.

### Drafting of Standards

Leads for each assigned topics is responsible for draft their assigned standard.

This same process applies in the case where a Standard exists but is either
effectively amended or supersedes another.

#### Research

The Lead shall track all issues that need addressing in the Standard, and be
prepared to produce reports thereof upon request from the Board.

#### Initial Draft

The Lead will draft and edit the Standard.

#### Peer Review

Each member of the DC will review drafts and submit revision suggestions.

#### Committee Revision

The Lead is responsible to integrate according to their own discretion the
reviewers' suggestions.

Once all revision issues are addressed--the draft is refered to as a
"Candidate Draft."

#### Working Group Review

The Candidate Draft is posted to the Working Group for review.

#### Working Group Revision

The Lead is responsible for capturing, discussing and resolving issues raised
by the Working Group.

#### Submit as TZIP

Once all issues raised by the Working Group are resolved, the Lead presents
the Candidate Draft to the Board for approval.

Once approved, the TZIP is submitted through formal channels of the TF.

### Drafting of Guidelines or Other Support Material

The Process for Guidelines or Other Support Materials is similar to the Drafting
of Standards, with the important difference that it is not submitted to the TF
as a TZIP, it is instead reference on the main README of the WWG's repository.
